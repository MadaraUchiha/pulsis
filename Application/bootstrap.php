<?php
/**
 * @author Dor Tchizik <dor.tchizik@gmail.com>
 * @package Pulsis
 */

use Pulsis\Application\Common\SplClassLoader;

require "Common/Autoloader.php";

$autoloader = new SplClassLoader("\\Pulsis", realpath("../../"));
$autoloader->register();

